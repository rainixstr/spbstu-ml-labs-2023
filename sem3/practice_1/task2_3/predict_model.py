from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
import pandas as pd
import pickle

data = pd.read_csv('car_data.csv')

x = data.drop(['Car_Name', 'Selling_Price'], axis=1)
y = data['Selling_Price']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.24, random_state=42)

model = RandomForestRegressor()
model.fit(x_train, y_train)

with open("model.pkl", "wb") as f:
    pickle.dump(model, f)


def predict_car_price(inp_data):
    new_data = pd.DataFrame({
        'Present_Price': inp_data[0],
        'Kms_Driven': inp_data[1],
        'Fuel_Type': inp_data[2],
        'Seller_Type': inp_data[3],
        'Transmission': inp_data[4],
        'Age': inp_data[5]
    }, index=[0])
    return model.predict(new_data)
