from flask import Flask, render_template, request, redirect, url_for, session
from predict_model import predict_car_price
import datetime

app = Flask(__name__)
app.secret_key = 'ml-2023'


@app.route('/')
def index():
    print('logged_in' not in session)
    if 'logged_in' not in session:
        return redirect(url_for('login'))
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        if username == 'admin' and password == 'admin':
            session['logged_in'] = True
            return redirect(url_for('index'))
        else:
            return render_template('login.html', error='Invalid login or password')

    return render_template('login.html')


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('login'))


@app.route("/predict", methods=["POST", "GET"])
def predict():
    if request.method == "POST":
        price = float(request.form['price'])
        kms = float(request.form['kms'])
        fuel_name = request.form['fuel']
        seller_name = request.form['seller']
        mode_name = request.form['mode']
        year = request.form['year']
        current_year = datetime.datetime.now().year
        age = current_year - int(year)

        fuel = 2 if fuel_name == 'CNG' else 1 if fuel_name == 'Diesel' else 0
        seller = 0 if seller_name == 'Dealer' else 1
        mode = 0 if mode_name == 'Manual' else 1

        prediction = predict_car_price([price, kms, fuel, seller, mode, age])

    return render_template("result.html", prediction=float(prediction).__round__(3))


if __name__ == "__main__":
    app.run(debug=True)
